#include <bluefruit.h>
#include <Adafruit_LittleFS.h>
#include <InternalFileSystem.h>
#include <Wire.h>
#include "Adafruit_MPU6050.h"
#include "MLP.h"

const char bleName[9] = "Kevin000"; //BLE Device Name 只能8個字元
uint8_t packetADV[31];
uint8_t bleMacAddress[6];
BLEUart bleuart; // uart over ble

Adafruit_MPU6050 mpu;  //設定加速規
double ax, ay, az;
float x, y, z;
float pre_x, pre_y, pre_z;
double input[16]; //資料暫存
unsigned long timeNew, timeOld;
const float threshold = 0.1;

#define NET_INPUTS 15   //MLP輸入層
#define NET_OUTPUTS 3   //MLP輸出層
int layerSizes[] = {9, NET_OUTPUTS, -1}; //MLP隱藏層
int event;
MLP mlp(NET_INPUTS, NET_OUTPUTS, layerSizes, MLP::LOGISTIC, false);

void setup() {
  // put your setup code here, to run once
  Serial.begin(115200);
  // Set up and start advertising
  startAdv();

  Serial.println("Adafruit MPU6050 Initialization!");
  // verify MPU6050 connection
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Start!");
  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
}

void startAdv(void) {
  // Setup the BLE LED to be enabled on CONNECT
  // Note: This is actually the default behavior, but provided
  // here in case you want to control this LED manually via PIN 19
  Bluefruit.autoConnLed(true);

  // Config the peripheral connection with maximum bandwidth
  // more SRAM required by SoftDevice
  // Note: All config***() function must be called before begin()
  Bluefruit.configPrphBandwidth(BANDWIDTH_MAX);

  Bluefruit.begin();
  Bluefruit.setName(bleName);
  Bluefruit.getAddr(bleMacAddress);
  Serial.printf("BLE MAC address: %02X:%02X:%02X:%02X:%02X:%02X\r\n", bleMacAddress[5], bleMacAddress[4], bleMacAddress[3], bleMacAddress[2], bleMacAddress[1], bleMacAddress[0]);

  Bluefruit.setTxPower(4);    // Check bluefruit.h for supported values
  Bluefruit.Periph.setConnectCallback(connect_callback);
  Bluefruit.Periph.setDisconnectCallback(disconnect_callback);

  packetADV[0] = 0x02; packetADV[1] = 0x01; packetADV[2] = 0x06;

  packetADV[3] = 0x09; packetADV[4] = 0x09;
  memcpy(packetADV + 5, bleName, 8);
  Bluefruit.Advertising.setData(packetADV, sizeof(packetADV));

  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds

  // Configure and Start BLE Uart Service
  bleuart.begin();

  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);
}

void loop() {
  // 取加速規x, y, z的值
  getAcceleration();

  //  加速規整理判斷
  if (abs(x - pre_x) > threshold && abs(y - pre_y) > threshold && abs(z - pre_z) > threshold ) {
    timeOld = millis();
    byte counts = 0;
    while (counts < 5) {
      timeNew = millis();
      if (timeNew - timeOld >= 200) {
        getAcceleration();
        input[3 * counts + 1] = x; input[3 * counts + 2] = y; input[3 * counts + 3] = z;
        counts++;
        timeOld = timeNew;
      }
      if (counts == 5) {
        input[0] = -1;
        event = mlp.getActivation(input);
        Serial.println(event);
        bleuart.write(event);
      }
    }

    for (int i = 1; i < 15; i++) {
      Serial.print(input[i]); Serial.print(",");
    }
    Serial.println(input[15]);
    pre_x = x; pre_y = y; pre_z = z;
  }
}

void getAcceleration() {
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);
  //accelgyro.getAcceleration(&ax, &ay, &az);
  //convert to -1 ~ 1
  x = a.acceleration.x / 78.45;
  y = a.acceleration.y / 78.45;
  z = a.acceleration.z / 78.45;
}

// callback invoked when central connects
void connect_callback(uint16_t conn_handle) {
  // Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));

  Serial.print("Connected to ");
  Serial.println(central_name);
}

/**
   Callback invoked when a connection is dropped
   @param conn_handle connection where this event happens
   @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
*/
void disconnect_callback(uint16_t conn_handle, uint8_t reason) {
  (void) conn_handle;
  (void) reason;

  Serial.println();
  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);
}

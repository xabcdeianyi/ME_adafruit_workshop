
#ifndef MLP_h
#define MLP_h

#include "Arduino.h"
#include "Weight.h"

class MLP {
  public:
    MLP(byte inputs, byte outputs, int *topology, byte activFunc, boolean usingPGM);

    /**
       Logistic activation function
    */
    static const byte LOGISTIC;

    /**
       Hyperbolic Tangent activation function
    */
    static const byte TANH;

    /**
       Linear activation function
    */
    static const byte LINEAR;

    double* forward(double *sample);
    int getActivation(double *sample);

  private:
    byte _inputs, _outputs, _layers, _iOffset, _yOffset, _activFunc;
    boolean _PGM;
    int* _layerSizes;
    double _inflection;
    
    double* _iArray;
    double* _yArray;

    double activation(double x);
    double getWeight(int layer, int neuron, int weight, int inputs);
    double* getI(int inputs, int layer, double *sample);
    double* getY(int inputs, int layer, double *sample);

};

#endif

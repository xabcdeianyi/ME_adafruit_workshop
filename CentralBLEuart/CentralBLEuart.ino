#include <Adafruit_NeoPixel.h>
#include <bluefruit.h>
BLEClientUart clientUart; // bleuart client

#define myBleDevice "Kevin888"

Adafruit_NeoPixel pixels(3, 24, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(115200);
  
  pinMode(23, OUTPUT);
  digitalWrite(23, HIGH);

  pixels.begin();
  
  Serial.println("Bluefruit52 Central BLEUART Example");
  Serial.println("-----------------------------------\n");
  
  // Initialize Bluefruit with maximum connections as Peripheral = 0, Central = 1
  // SRAM usage required by SoftDevice will increase dramatically with number of connections
  Bluefruit.begin(0, 1);

  Bluefruit.setName("ME301");

  // Init BLE Central Uart Serivce
  clientUart.begin();
  clientUart.setRxCallback(bleuart_rx_callback);

  // Callbacks for Central
  Bluefruit.Central.setConnectCallback(connect_callback);
  Bluefruit.Central.setDisconnectCallback(disconnect_callback);

  Bluefruit.Scanner.setRxCallback(scan_callback);
  Bluefruit.Scanner.restartOnDisconnect(true);
  Bluefruit.Scanner.setInterval(160, 80); // in unit of 0.625 ms
  Bluefruit.Scanner.useActiveScan(true);
  Bluefruit.Scanner.start(0);                   // // 0 = Don't stop scanning after n seconds
}

void scan_callback(ble_gap_evt_adv_report_t* report) {
  bool myBLE = false;
  uint8_t adv_name[32];
  memset(adv_name, 0, sizeof(adv_name));

  if (Bluefruit.Scanner.parseReportByType(report, BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME, adv_name, sizeof(adv_name))) {
    if (memcmp(myBleDevice, adv_name, 8) == 0x00) {
      Serial.printf("%14s %s\n", "COMPLETE NAME:", adv_name);
      myBLE = true;
      memset(adv_name, 0, sizeof(adv_name));
    }
  }

  if (myBLE) {
    // Connect to device with bleuart service in advertising
    Bluefruit.Central.connect(report);
  }
  else {
    // For Softdevice v6: after received a report, scanner will be paused
    // We need to call Scanner resume() to continue scanning
    Bluefruit.Scanner.resume();
  }
}

/**
   Callback invoked when an connection is established
   @param conn_handle
*/
void connect_callback(uint16_t conn_handle) {
  Serial.println("Connected");

  Serial.print("Discovering BLE Uart Service ... ");
  if ( clientUart.discover(conn_handle) ) {
    Serial.println("Found it");

    Serial.println("Enable TXD's notify");
    clientUart.enableTXD();

    Serial.println("Ready to receive from peripheral");
  }
  else {
    Serial.println("Found NONE");

    // disconnect since we couldn't find bleuart service
    Bluefruit.disconnect(conn_handle);
  }
}

void disconnect_callback(uint16_t conn_handle, uint8_t reason) {
  (void) conn_handle;
  (void) reason;

  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);
}

void bleuart_rx_callback(BLEClientUart & uart_svc) {
  int value = uart_svc.read();
  Serial.print("Read:");
  Serial.println(value);
  if (value == 0) {
    pixels.setPixelColor(0, pixels.Color(150, 0, 0));
    pixels.show();
  } else if (value == 1) {
    pixels.setPixelColor(0, pixels.Color(0, 150, 0));
    pixels.show();
  } else if (value == 2) {
    pixels.setPixelColor(0, pixels.Color(0, 0, 150));
    pixels.show();
  } else {
    pixels.setPixelColor(0, pixels.Color(0, 0, 0));
    pixels.show();
  }
}

void loop() {
  if ( Bluefruit.Central.connected() ) {
    // Not discovered yet
    if ( clientUart.discovered() ) {
      // Discovered means in working state
      // Get Serial input and send to Peripheral
      if ( Serial.available() ) {
        delay(2); // delay a bit for all characters to arrive

        char str[20 + 1] = { 0 };
        Serial.readBytes(str, 20);

        clientUart.print( str );
      }
    }
  }
}
